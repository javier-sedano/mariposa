THIS PROJECT IS ABANDONED. IT IS KEPT HERE ONLY FOR REFERENCE. Using Django for an application like this one (no database, no users, no roles) is a huge overkill, leading to a lot of wasted time dealing with removing than unused features.

Reference architecture of public web application with mobile-first responsive design based on Python/Django and Vue with CI in Gitlab-CI and CD to Google Compute Engine.

VERY EARLY ALPHA STAGE

Master branch:
[![pipeline status](https://gitlab.com/javier-sedano/mariposa/badges/master/pipeline.svg)](https://gitlab.com/javier-sedano/mariposa/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.mariposa&metric=alert_status)](https://sonarcloud.io/dashboard?id=com.odroid.mariposa)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.mariposa&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=com.odroid.mariposa)
<details>
<summary>Quality details</summary>

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.mariposa&metric=bugs)](https://sonarcloud.io/dashboard?id=com.odroid.mariposa)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.mariposa&metric=code_smells)](https://sonarcloud.io/dashboard?id=com.odroid.mariposa)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.mariposa&metric=coverage)](https://sonarcloud.io/dashboard?id=com.odroid.mariposa)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.mariposa&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=com.odroid.mariposa)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.mariposa&metric=ncloc)](https://sonarcloud.io/dashboard?id=com.odroid.mariposa)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.mariposa&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=com.odroid.mariposa)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.mariposa&metric=security_rating)](https://sonarcloud.io/dashboard?id=com.odroid.mariposa)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.mariposa&metric=sqale_index)](https://sonarcloud.io/dashboard?id=com.odroid.mariposa)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.mariposa&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=com.odroid.mariposa)

[Sonar analysis](https://sonarcloud.io/dashboard?id=com.odroid.mariposa)
</details>

Keywords: python, django, jest, sonarqube, vue, vscode, devcontainer, docker, yargs, nodemon, concurrently, w3css, fontawesome, giphy

Instructions:
* Suggested environment:
  * Visual Studio Code. Plugins:
    * Docker
    * Remote - Containers
  * Docker. Docker Desktop for Windows is tested
* Open VSCode and use `F1` --> `Remote containers: Clone Repository in Container Volume...` --> Paste Git clone URL --> `Create a new volume...` (or choose an existing one) --> Accept default name
  * Remote container will be detected and automatically used.
* Makefile targets will be shown in Task Explorer
  * Run "dependenciesInit" to download dependencies (command line: make dependenciesInit)
  * Run "devStart" to start serving (while watching for changes in both frontend and backend) (command line: make devStart)
    * Most common development tasks are configured as Makefile targets
  * Browse http://localhost:4013/

Useful links:

* https://code.visualstudio.com/docs/remote/containers
* https://code.visualstudio.com/docs/editor/tasks
* https://www.gnu.org/software/make/manual/make.html
* https://www.python.org/
* https://www.djangoproject.com/
* https://vuejs.org/
* https://cli.vuejs.org/
* https://router.vuejs.org/
* https://jestjs.io/
* https://github.com/axios/axios
* https://www.w3schools.com/w3css/
* https://fontawesome.com/
* https://developers.giphy.com/docs/api/endpoint/#random
